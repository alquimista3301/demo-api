import unittest
from hasMutation import hasMutation


class TestHasMutation(unittest.TestCase):

    def test_no_mutation(self):
        dna = ["ATGCGA", "CAGTGC", "TTATTT", "AGACGG", "GCGTCA", "TCACTG"]
        self.assertFalse(hasMutation(dna))

    def test_horizontal_mutation(self):
        dna = ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]
        self.assertTrue(hasMutation(dna))

    def test_vertical_mutation(self):
        dna = ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "TCCCTA", "TCACTG"]
        self.assertTrue(hasMutation(dna))

    def test_diagonal_left_to_right_mutation(self):
        dna = ["ATGCGA", "CAGTGC", "TTATGT", "AGATGG", "CCCTTA", "TCACTG"]
        self.assertTrue(hasMutation(dna))

    def test_diagonal_right_to_left_mutation(self):
        dna = ["ATGCGA", "CAGTGC", "TTGTGT", "AGACGG", "GCGTCA", "TCACTG"]
        self.assertFalse(hasMutation(dna))

if __name__ == '__main__':
    unittest.main()