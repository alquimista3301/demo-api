
def hasMutation(dna):
    n = len(dna)
    count = 0
    # Verificar secuencias horizontales
    for i in range(n):
        for j in range(n-3):
            if dna[i][j:j+4] == dna[i][j]*4:
                count += 1
                if count > 1:
                    return True

    # Verificar secuencias verticales
    for i in range(n-3):
        for j in range(n):
            if dna[i][j] == dna[i+1][j] == dna[i+2][j] == dna[i+3][j]:
                count += 1
                if count > 1:
                    return True

    # Verificar secuencias diagonales de izquierda a derecha
    for i in range(n-3):
        for j in range(n-3):
            if dna[i][j] == dna[i+1][j+1] == dna[i+2][j+2] == dna[i+3][j+3]:
                count += 1
                if count > 1:
                    return True

    # Verificar secuencias diagonales de derecha a izquierda
    for i in range(n-3):
        for j in range(3, n):
            if dna[i][j] == dna[i+1][j-1] == dna[i+2][j-2] == dna[i+3][j-3]:
                count += 1
                if count > 1:
                    return True

    return False
