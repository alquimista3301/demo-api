# Conditional (O(n)2)
def hasMutation(dna):
    n = len(dna)
    for i in range(n):
        for j in range(n):
            # Horizontal
            if j < n-3 and dna[i][j] == dna[i][j+1] == dna[i][j+2] == dna[i][j+3]:
                return True
            # Vertical
            if i < n-3 and dna[i][j] == dna[i+1][j] == dna[i+2][j] == dna[i+3][j]:
                return True
            # Diagonal derecha
            if i < n-3 and j < n-3 and dna[i][j] == dna[i+1][j+1] == dna[i+2][j+2] == dna[i+3][j+3]:
                return True
            # Diagonal izquierda
            if i < n-3 and j > 2 and dna[i][j] == dna[i+1][j-1] == dna[i+2][j-2] == dna[i+3][j-3]:
                return True
    return False

# def hasMutation(dna):
#     n = len(dna)
#     sequences = set()

#     def check_sequence(s):
#         if s in sequences:
#             return True
#         else:
#             sequences.add(s)
#             return False
#     # Verificar secuencias horizontales
#     for i in range(n):
#         for j in range(n-3):
#             if check_sequence(dna[i][j:j+4]):
#                 if len(sequences) == 2:
#                     return True
#     # Verificar secuencias verticales
#     for i in range(n-3):
#         for j in range(n):
#             if check_sequence(dna[i][j] + dna[i+1][j] + dna[i+2][j] + dna[i+3][j]):
#                 if len(sequences) == 2:
#                     return True
#     # Verificar secuencias diagonales de izquierda a derecha
#     for i in range(n-3):
#         for j in range(n-3):
#             if check_sequence(dna[i][j] + dna[i+1][j+1] + dna[i+2][j+2] + dna[i+3][j+3]):
#                 if len(sequences) == 2:
#                     return True
#     # Verificar secuencias diagonales de derecha a izquierda
#     for i in range(n-3):
#         for j in range(3, n):
#             if check_sequence(dna[i][j] + dna[i+1][j-1] + dna[i+2][j-2] + dna[i+3][j-3]):
#                 if len(sequences) == 2:
#                     return True
#     return False

# Con mutacion
dnaTest1 = [
    "ATGCGA", 
    "CAGTGC", 
    "TTATGT", 
    "AGAAGG", 
    "CCCCTA",
    "TCACTG"
]
# Sin mutacion
dnaTest2 = [
    "ATGCGA",
    "CAGTGC",
    "TTATTT",
    "AGACGG",
    "GCGTCA",
    "TCACTG"
]

print(hasMutation(dnaTest1))